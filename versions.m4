m4_define(`VERSION',`4.4.0')
m4_define(`DL_DATE',`2024-11-27')
m4_define(`DOWNLOAD_URL',`https://files.gpg4win.org/gpg4win-VERSION.exe')
m4_define(`DL_SIZE',`35 MByte')
m4_define(`SDOWNLOAD_URL',`https://files.gpg4win.org/gpg4win-VERSION.tar.xz')
m4_define(`SDL_SIZE',`251 MByte')

m4_dnl Paste output of mk-chksums here
m4_define(`SHA1_SRC',`970acd62bc190fbbcc913bc37d9128a61c6a42c6')
m4_define(`SHA1_EXE',`5c5e06f9f36d816ba14847d813127c9510836394')
m4_define(`SHA2_SRC',`dbdbc14b60cc7bff92ea4103484202ef6fcac068838676fdd08e3e464dcb4b10')
m4_define(`SHA2_EXE',`765673854c1503602b09c97bfa6c72b534e2414185fb2f23a0ce19cf8cecd891')
m4_define(`LEN_SRC',`262787380')
m4_define(`LEN_EXE',`36001528')
m4_dnl end paste

m4_define(`DLPAGE',`I18N(`EN',`download.html')`'I18N(`DE',`download-de.html')')
m4_define(`SOFTWAREOVERVIEW',`
I18N(`EN', `<a href="https://www.gnupg.org/">GnuPG</a>')I18N(`DE', `<a href="https://www.gnupg.org/index.de.html">GnuPG</a>') 2.4.7
<a href="https://www.kde.org/applications/utilities/kleopatra/">Kleopatra</a> 3.3.0
<a href="https://git.gnupg.org/cgi-bin/gitweb.cgi?p=gpgol.git;a=summary">GpgOL</a> 2.5.14
<a href="https://git.gnupg.org/cgi-bin/gitweb.cgi?p=gpgex.git;a=summary">GpgEX</a> 1.0.11
<a href="https://okular.kde.org">Okular</a> 23.11.70-patched
<a href="doc/de/gpg4win-compendium.html">Kompendium (de)</a> 4.0.1
<a href="doc/en/gpg4win-compendium.html">Compendium (en)</a> 3.0.0
')


m4_define(`COMPENDIUM_VERSION_DE',`4.0.1')
m4_define(`COMPENDIUM_DATE_DE',`2018-04-03')
m4_define(`DOWNLOAD_URL_COMPENDIUM_DE',`https://files.gpg4win.org/doc/gpg4win-compendium-de.pdf')
m4_define(`DL_SIZE_COMPENDIUM_DE',`3 MByte')
m4_define(`COMPENDIUM_VERSION_EN',`3.0.0')
m4_define(`COMPENDIUM_DATE_EN',`2016-11-30')
m4_define(`DOWNLOAD_URL_COMPENDIUM_EN',`https://files.gpg4win.org/doc/gpg4win-compendium-en.pdf')
m4_define(`DL_SIZE_COMPENDIUM_EN',`3 MByte')

m4_define(`BETA_VERSION', `5.0.0-beta103')
m4_define(`BETA_SIZE', `43 MByte')
