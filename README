----------------------------------------------------------------------
                           gpg4win Web Site
----------------------------------------------------------------------

INTRODUCTION:

Getting the source:

  git clone  git://git.gnupg.org/gpg4win-website.git


File organisation:

The .html files are build from the *.htm4 files using GNU m4.  The
*.htm4 files consist of mainly simple plain HTML and some macros
implementing the overall page layout.

The structure of the .htm4 files should be rather self explaining.


BUILDING:

WARNING: you might need a latin1 locale for m4 to work correctly.

To build to web site do this:

   make


INSTALLING:

To install the generated web site you need a gpg4win docwriter or
developer account on wald.intevation.org.

Installing is done from within the root directory,
just type:

  make online

You will have to enter the ssh password of your account at wald.intevation.org.

If you are interested on web site management on wald.intevation.org
in general, please read the online manual:
http://wald.intevation.org/docman/view.php/1/34/project-websites.txt

If you need the website as offline version for some other
purposes just run:

  make tar

After thar you have the file "www.gpg4win.org.tar.gz".

TEMPLATES:

versions.m4 : Definitions of some macros, holding the current versions
              of gpg4win and the included software packages.  If any
              version changes this file should be changed accordingly.
              This file is included by `template_header.m4'.
              The current version numbers of the packages installed by
              gpg4win can be found here:
              ftp://ftp.gpg4win.org/gpg4win/packages.current

template.m4 : Contains the definition of the main macros PAGE_START
              and PAGE_BOXES as well as some helper macros normally
              not used in the .htm4 files.  Authors should not need to
              change this file.

template_header.m4: Contains the actual page layout.  This file is
                    included by `template.m4'.  Authors should not
                    need to change this file, but if additional
                    languages are added the navigation must be added
                    in here.
